package org.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages = { "org.module.delevery", "org.module.customer" })
public class VendorApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(VendorApplication.class, args);
		System.out.println("======================= Application is loaded As JAR File===================== ");
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		System.out.println("===================== Application is loaded As WAR File===================== ");
		return builder.sources(VendorApplication.class);

	}
}