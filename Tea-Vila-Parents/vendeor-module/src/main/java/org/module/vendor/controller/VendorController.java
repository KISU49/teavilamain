package org.module.vendor.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
@RestController
@RequestMapping(value= {"/","/vendor"})
public class VendorController{
 
	@Value("${teavila.name}")
	private String name;
	
	@GetMapping
	public String testMessage() {
		return name+" = Vendor Module";
	}

}