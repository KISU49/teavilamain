package org.module.customer.service;

import org.module.customer.model.Customer;
import org.module.customer.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer getCustomerByEMail(String email) {
		 Customer c = customerRepository.findByEmail(email);
		return c;
	}

}
