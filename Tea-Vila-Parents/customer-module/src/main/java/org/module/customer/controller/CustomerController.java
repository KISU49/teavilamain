package org.module.customer.controller;

import org.module.customer.model.Customer;
import org.module.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("/getCustomerByemail")
	public @ResponseBody Customer getCustomer(@Param("email") String email) {
		Customer c = customerService.getCustomerByEMail(email);
		return c;

	}
}