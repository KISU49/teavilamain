package org.module.customer.repository;

import org.module.customer.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, Long>{

	public Customer findByEmail(String email);
}