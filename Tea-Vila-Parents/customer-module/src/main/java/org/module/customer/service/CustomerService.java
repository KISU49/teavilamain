package org.module.customer.service;

import org.module.customer.model.Customer;
import org.springframework.stereotype.Service;


public interface CustomerService {

	public Customer getCustomerByEMail(String email);
}