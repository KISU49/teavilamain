package org.module.delevery.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
@RestController
@RequestMapping("/delevery")
public class DeleveryController {
	@GetMapping
	public String deleveryMessage() {
		return "delevery Module loaded successfully";
	}
}